﻿using System;

namespace test1.Models
{
    public class Pin
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public bool Enabled { get; set; }
    }
}