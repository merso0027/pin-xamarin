﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using test1.Models;

namespace test1.Services
{
    public class MockDataStore : IDataStore<Pin>
    {
        readonly List<Pin> items;

        public MockDataStore()
        {
            items = new List<Pin>()
            {
                new Pin { Id = Guid.NewGuid().ToString(), Name = "First pin", Value="Pin value",Enabled=true }
            };
        }

        public async Task<bool> AddItemAsync(Pin item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Pin item)
        {
            var oldItem = items.Where((Pin arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((Pin arg) => arg.Id == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Pin> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Pin>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }
    }
}