﻿using System;

using test1.Models;

namespace test1.ViewModels
{
    public class PinViewModel : BaseViewModel
    {
        public Pin Item { get; set; }
        public PinViewModel(Pin item = null)
        {
            Title = item?.Name;
            Item = item;
        }
    }
}
