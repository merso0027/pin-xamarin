﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using test1.Models;
using test1.ViewModels;

namespace test1.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class ItemDetailPage : ContentPage
    {
        PinViewModel viewModel;

        public ItemDetailPage(PinViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
        }

        public ItemDetailPage()
        {
            InitializeComponent();

            var item = new Pin
            {
                Name = "",
                Value = "",
                Enabled = false
            };

            viewModel = new PinViewModel(item);
            BindingContext = viewModel;
        }
    }
}